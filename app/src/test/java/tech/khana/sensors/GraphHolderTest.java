package tech.khana.sensors;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;

import tech.khana.sensors.db.entities.SensorData;
import tech.khana.sensors.view.GraphHolder;

public class GraphHolderTest {

    @Test
    public void minByTimestamp() {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        long expectedMin = Long.MAX_VALUE;
        Collection<SensorData> sensorData = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            long nextLong = random.nextLong();
            expectedMin = nextLong > expectedMin ? expectedMin : nextLong;
            sensorData.add(new SensorData(nextLong, (byte) 0, 0f, null));
        }

        long actualMin = GraphHolder.minByTimeStamp(sensorData);

        Assert.assertEquals(expectedMin, actualMin);
    }


}
