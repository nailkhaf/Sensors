package tech.khana.sensors;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import tech.khana.sensors.db.dao.SensorDataDao;
import tech.khana.sensors.db.entities.SensorData;
import tech.khana.sensors.service.EventValue;
import tech.khana.sensors.service.SensorEventListenerImpl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static tech.khana.sensors.DescriptionGraphs.PERIOD_COMPUTE_AVG;
import static tech.khana.sensors.service.EventValue.createEventValue;

public class SensorEventListenerImplTest {

//    @Test
//    public void saveInBufferOrSendNext_checkThatProcessAndStoreNewDataOnceInPeriod() {
//        ThreadLocalRandom random = ThreadLocalRandom.current();
//        String sensorName = "test_sensor";
//        byte countMeasurement = 3;
//
//        float[] values = new float[countMeasurement];
//        for (int i = 0; i < values.length; i++) {
//            values[i] = random.nextFloat();
//        }
//
//        @SuppressWarnings("unchecked")
//        List<SensorData> expectedData = mock(List.class);
//        SensorDataDao db = mock(SensorDataDao.class);
//
//        SensorEventListenerImpl listener = spy(new SensorEventListenerImpl(null, sensorName, countMeasurement, db));
//
//        doReturn(expectedData).when(listener).processSensorEvents(anyList(), anyLong());
//
//        final int EVENT_PERIOD = 100;
//        for (int i = 0; i <= PERIOD_COMPUTE_AVG / EVENT_PERIOD; i++) {
//            EventValue sensorEvent = createEventValue((long) (i * EVENT_PERIOD) * 1_000_000, values);
//
//            listener.saveInBufferOrSendNext(sensorEvent);
//        }
//
//        verify(db).insert(eq(expectedData));
//    }

//    @Test
//    public void processSensorEvent_checkJavaProcessingData() {
//        String sensorName = "test_sensor";
//        byte countMeasurement = 3;
//        int currentNumberPeriod = 12345;
//        long timeStamp = 1000L;
//        float expectedAvg = 3.14f;
//        List<SensorData> expectedSensorData = Arrays.asList(
//                new SensorData(currentNumberPeriod * PERIOD_COMPUTE_AVG, (byte) 0, expectedAvg, sensorName),
//                new SensorData(currentNumberPeriod * PERIOD_COMPUTE_AVG, (byte) 1, expectedAvg, sensorName),
//                new SensorData(currentNumberPeriod * PERIOD_COMPUTE_AVG, (byte) 2, expectedAvg, sensorName)
//        );
//
//        float[] values = new float[]{1.12f, 3.22f, 2.4f}; // countMeasurement = 3
//
//        SensorDataDao db = mock(SensorDataDao.class);
//
//        SensorEventListenerImpl listener = spy(new SensorEventListenerImpl(null, sensorName, countMeasurement, db));
//
//        List<EventValue> eventValues = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//            eventValues.add(EventValue.createEventValue(timeStamp, values));
//        }
//
//        doReturn(expectedAvg).when(listener).computeAvg(any(float[].class));
//
//        List<SensorData> actualSensorData = listener.processSensorEvents(eventValues, currentNumberPeriod);
//
//        Assert.assertEquals(expectedSensorData, actualSensorData);
//    }
}
