#include <jni.h>

extern "C" JNIEXPORT jfloat JNICALL
Java_tech_khana_sensors_service_SensorEventListenerImpl_computeAvg(JNIEnv *env, jobject instance,
                                                     jfloatArray array_) {
    jfloat *array = env->GetFloatArrayElements(array_, NULL);

    jsize len = env->GetArrayLength(array_);

    jfloat avg = 0;
    for (int i = 0; i < len; ++i) {
        avg += array[i];
    }
    avg = avg / len;

    env->ReleaseFloatArrayElements(array_, array, 0);
    return avg;
}