package tech.khana.sensors;

import android.app.Application;
import android.arch.persistence.room.Room;

import tech.khana.sensors.db.SensorDatabase;

public class App extends Application {
    private SensorDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();
        db = Room.databaseBuilder(getApplicationContext(), SensorDatabase.class, "sensor_data")
                .build();

    }

    public SensorDatabase getDb() {
        return db;
    }
}
