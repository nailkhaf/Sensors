package tech.khana.sensors.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import tech.khana.sensors.db.entities.SensorData;

@Dao
public interface SensorDataDao {
    @Insert
    void insert(List<SensorData> list);

    @Query("select * from sensordata")
    List<SensorData> getAll();

    @Query("delete from sensordata")
    void deleteAll();

    @Query("select * from sensordata " +
            "where timestamp > :timeAfter ")
    Flowable<List<SensorData>> getAllAfter(long timeAfter);

    // todo BackpressureStrategy.LATEST
    @Query("select * from sensordata " +
            "where timestamp > :timeAfter " +
            "order by id desc " +
            "limit :limit ")
    Flowable<List<SensorData>> getSensorData(long timeAfter, int limit);

}
