package tech.khana.sensors.db.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.Objects;

@Entity(indices = {@Index("timestamp"), @Index("sensorName")})
public class SensorData {
    @PrimaryKey(autoGenerate = true)
    private long id;

    private long timestamp;
    private byte type;
    private float value;

    // need to add foreign key
    private String sensorName;

    public SensorData() {
    }

    public SensorData(long timestamp, byte type, float value, String sensorName) {
        this.timestamp = timestamp;
        this.type = type;
        this.value = value;
        this.sensorName = sensorName;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SensorData that = (SensorData) o;
        return id == that.id &&
                timestamp == that.timestamp &&
                type == that.type &&
                Float.compare(that.value, value) == 0 &&
                Objects.equals(sensorName, that.sensorName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, type, value, sensorName);
    }
}
