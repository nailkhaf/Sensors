package tech.khana.sensors.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import tech.khana.sensors.db.dao.SensorDataDao;
import tech.khana.sensors.db.entities.SensorData;

@Database(entities = {SensorData.class}, version = 1, exportSchema = false)
public abstract class SensorDatabase extends RoomDatabase {
    public abstract SensorDataDao sensorDataDao();
}
