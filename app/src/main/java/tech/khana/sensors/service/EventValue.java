package tech.khana.sensors.service;

public interface EventValue {

    float[] getValues();

    long getTimestamp();

    static EventValue createEventValue(long timeStamp, float[] values) {
        return new EventValue() {
            @Override
            public float[] getValues() {
                return values;
            }

            @Override
            public long getTimestamp() {
                return timeStamp;
            }
        };
    }
}
