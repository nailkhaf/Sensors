package tech.khana.sensors.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import tech.khana.sensors.App;
import tech.khana.sensors.DescriptionGraphs;
import tech.khana.sensors.db.SensorDatabase;

public class BackgroundService extends Service {
    private SensorManager sensorManager;
    private List<SensorEventListener> listeners = new ArrayList<>();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(this, "service created", Toast.LENGTH_SHORT).show();
        SensorDatabase db = ((App) getApplicationContext()).getDb();
        Executor executor = Executors.newSingleThreadExecutor();

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager == null) {
            throw new IllegalStateException("Service sensor manager not found!");
        }
        for (int i = 0; i < DescriptionGraphs.sensorTypes.length; i++) {
            Sensor sensor = sensorManager.getDefaultSensor(DescriptionGraphs.sensorTypes[i]);

            SensorEventListenerImpl listener = new SensorEventListenerImpl(executor, DescriptionGraphs.sensorNames[i], DescriptionGraphs.countDataType[i],
                    db.sensorDataDao());
            listeners.add(listener);
            boolean success = sensorManager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_NORMAL);

            if (!success)
                throw new IllegalStateException("it looks like you do not have the needed sensor");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (SensorEventListener listener : listeners) {
            sensorManager.unregisterListener(listener);
        }
        if (!listeners.isEmpty()) listeners.clear();
    }
}
