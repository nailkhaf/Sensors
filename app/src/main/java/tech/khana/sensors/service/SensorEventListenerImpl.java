package tech.khana.sensors.service;

import android.annotation.SuppressLint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import tech.khana.sensors.db.dao.SensorDataDao;
import tech.khana.sensors.db.entities.SensorData;

import static tech.khana.sensors.DescriptionGraphs.PERIOD_COMPUTE_AVG;
import static tech.khana.sensors.service.EventValue.createEventValue;

public class SensorEventListenerImpl implements SensorEventListener {
    private final Executor executor;

    private final String sensorName;
    private final byte countDataType;
    private final SensorDataDao sensorDataDao;

    private List<EventValue> bufferBySensorNameByType = new ArrayList<>();

    private volatile long sensorTimeReference = 0;
    private volatile long myTimeReference = 0;
    private Long lastNumberPeriod = null;

    static {
        try {
            System.loadLibrary("native-lib");
        } catch (UnsatisfiedLinkError error) {
            error.printStackTrace();
        }
    }

    public SensorEventListenerImpl(Executor executor, String sensorName, byte countDataType, SensorDataDao sensorDataDao) {
        this.executor = executor;
        this.sensorName = sensorName;
        this.countDataType = countDataType;
        this.sensorDataDao = sensorDataDao;
    }

    @SuppressLint("CheckResult")
    @Override
    public void onSensorChanged(SensorEvent event) {

        if (myTimeReference == 0) {
            myTimeReference = System.currentTimeMillis();
            sensorTimeReference = event.timestamp;
        }

        long currentNumberPeriod = (myTimeReference + (event.timestamp - sensorTimeReference)
                / 1000 / 1000) / PERIOD_COMPUTE_AVG;

        if (lastNumberPeriod == null) {
            lastNumberPeriod = currentNumberPeriod;
        }

        bufferBySensorNameByType.add(createEventValue(event.timestamp, event.values));

        if (currentNumberPeriod > lastNumberPeriod) {

            List<EventValue> sensorEvents = Collections.unmodifiableList(bufferBySensorNameByType);
            bufferBySensorNameByType = new ArrayList<>();

            Observable.just(sensorEvents)
                    .subscribeOn(Schedulers.computation())
                    .subscribe(data -> {
                        List<SensorData> sensorDataList = processSensorEvents(sensorEvents, currentNumberPeriod);
                        Collections.sort(sensorDataList, (o1, o2) -> Byte.compare(o2.getType(), o1.getType()));
                        sensorDataDao.insert(sensorDataList);
                    });

            lastNumberPeriod = currentNumberPeriod;
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // todo what is this
    }

    public List<SensorData> processSensorEvents(List<EventValue> sensorEvents, long currentNumberPeriod) {

        @SuppressLint("UseSparseArrays")
        Map<Byte, float[]> sensorEventsByType = new HashMap<>(); // x or y or z and etc
        for (int j = 0; j < sensorEvents.size(); j++) {
            EventValue sensorEvent = sensorEvents.get(j);
            for (byte i = 0; i < countDataType; i++) {

                float[] values = sensorEventsByType.get(i);
                if (values == null) values = new float[sensorEvents.size()];

                values[j] = sensorEvent.getValues()[i];
                sensorEventsByType.put(i, values);
            }
        }

        List<SensorData> sensorDataList = new ArrayList<>();
        for (Map.Entry<Byte, float[]> entry : sensorEventsByType.entrySet()) {
            float[] floatArray = entry.getValue();

            float avg = computeAvg(floatArray); // Native call

            SensorData sensorData = new SensorData();
            sensorData.setValue(avg);
            sensorData.setSensorName(sensorName);
            sensorData.setType(entry.getKey());
            sensorData.setTimestamp(currentNumberPeriod * PERIOD_COMPUTE_AVG);
            sensorDataList.add(sensorData);
        }
        return sensorDataList;
    }

    public native float computeAvg(float[] array);

}
