package tech.khana.sensors.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.Collection;
import java.util.LinkedHashMap;

import tech.khana.sensors.R;
import tech.khana.sensors.db.entities.SensorData;

public class GraphAdapter extends RecyclerView.Adapter<GraphViewHolder> {

    private final LinkedHashMap<String, GraphHolder> data;

    public GraphAdapter(LinkedHashMap<String, GraphHolder> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public GraphViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new GraphViewHolder(inflater.inflate(R.layout.item_graph, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull GraphViewHolder holder, int position) {
        String sensorName = data.keySet().toArray(new String[0])[position];
        GraphHolder graphHolder = data.get(sensorName);
        holder.apply(graphHolder);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void addSensorData(String sensorName, Collection<SensorData> sensorData) {
        GraphHolder graphHolder = data.get(sensorName);
        graphHolder.addSensorData(sensorData);
    }
}
