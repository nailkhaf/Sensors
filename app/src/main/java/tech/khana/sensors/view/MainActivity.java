package tech.khana.sensors.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import tech.khana.sensors.App;
import tech.khana.sensors.DescriptionGraphs;
import tech.khana.sensors.R;
import tech.khana.sensors.db.SensorDatabase;
import tech.khana.sensors.db.entities.SensorData;
import tech.khana.sensors.service.BackgroundService;

public class MainActivity extends AppCompatActivity {

    private GraphAdapter graphAdapter;
    private SensorDatabase db;
    private CompositeDisposable disposables = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = ((App) getApplicationContext()).getDb();

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager;
        if (getResources().getBoolean(R.bool.is_landscape)) {
            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        } else {
            layoutManager = new LinearLayoutManager(this);
        }
        recyclerView.setLayoutManager(layoutManager);

        LinkedHashMap<String, GraphHolder> holders = getGraphHolders();

        graphAdapter = new GraphAdapter(holders);
        recyclerView.setAdapter(graphAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startService(new Intent(this, BackgroundService.class));
    }

    private LinkedHashMap<String, GraphHolder> getGraphHolders() {
        LinkedHashMap<String, GraphHolder> holders = new LinkedHashMap<>();
        for (int j = 0; j < DescriptionGraphs.sensorNames.length; j++) {
            String sensorName = DescriptionGraphs.sensorNames[j];
            List<LineGraphSeries<DataPoint>> seriesList = new ArrayList<>();
            for (int i = 0; i < DescriptionGraphs.countDataType[j]; i++) {
                seriesList.add(newLineGraphSeries(DescriptionGraphs.labels[j][i],
                        DescriptionGraphs.colors[j][i]));
            }
            GraphHolder graphHolder = new GraphHolder(seriesList, sensorName);
            holders.put(sensorName, graphHolder);
        }
        return holders;
    }

    @Override
    protected void onResume() {
        super.onResume();

        long timeAfter = System.currentTimeMillis() - DescriptionGraphs.MAX_VISIBLE_PERIOD;

        disposables.add(db.sensorDataDao().getAllAfter(timeAfter)
                .firstElement()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::processNewDataAndSendToAdapter));

        disposables.add(db.sensorDataDao().getSensorData(timeAfter, DescriptionGraphs.sensorNames.length * 30) // todo experimental value
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::processNewDataAndSendToAdapter));
    }

    private void processNewDataAndSendToAdapter(List<SensorData> sensorDataList) {
        Map<String, Collection<SensorData>> mapBySensorName = Observable.fromIterable(sensorDataList)
                .toMultimap(SensorData::getSensorName)
                .blockingGet();

        Observable.fromIterable(mapBySensorName.entrySet()).blockingForEach(entry ->
                graphAdapter.addSensorData(entry.getKey(), entry.getValue()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        disposables.clear();
    }

    private LineGraphSeries<DataPoint> newLineGraphSeries(String label, int color) {
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
        series.setTitle(label);
        series.setColor(color);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(6);
        series.setThickness(4);
        series.setAnimated(true);
        return series;
    }

}
