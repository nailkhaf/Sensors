package tech.khana.sensors.view;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;

import tech.khana.sensors.R;

public class GraphViewHolder extends RecyclerView.ViewHolder {
    private final TextView textView;
    private final GraphView graphView;

    public GraphViewHolder(View itemView) {
        super(itemView);
        graphView = itemView.findViewById(R.id.graph);
        textView = itemView.findViewById(R.id.graph_title);
        graphView.getViewport().setScalable(true);
        graphView.getGridLabelRenderer().setVerticalLabelsVisible(false);
        graphView.getGridLabelRenderer().setHumanRounding(true);
        graphView.getLegendRenderer().setBackgroundColor(Color.LTGRAY);
        graphView.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.BOTTOM);
        graphView.getLegendRenderer().setVisible(true);
    }

    public void apply(GraphHolder graphHolder) {
        graphView.removeAllSeries();
        graphHolder.setGraphView(graphView);
        textView.setText(graphHolder.getSensorName());
    }
}
