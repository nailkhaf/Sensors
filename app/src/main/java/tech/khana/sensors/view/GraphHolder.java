package tech.khana.sensors.view;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.internal.util.ArrayListSupplier;
import io.reactivex.internal.util.HashMapSupplier;
import tech.khana.sensors.DescriptionGraphs;
import tech.khana.sensors.db.entities.SensorData;

public class GraphHolder {
    private final String sensorName;
    private final List<LineGraphSeries<DataPoint>> seriesList;
    private GraphView graphView;
    private long firstTimestamp = 0;
    private double counterTimestamp = 0;

    public GraphHolder(List<LineGraphSeries<DataPoint>> seriesList, String sensorName) {
        this.seriesList = seriesList;
        this.sensorName = sensorName;
    }

    public void setGraphView(GraphView graphView) {
        this.graphView = graphView;
        for (LineGraphSeries<DataPoint> series : seriesList) {
            this.graphView.addSeries(series);
        }
    }

    public String getSensorName() {
        return sensorName;
    }

    public static long minByTimeStamp(Collection<SensorData> sensorData) {
        long min = Long.MAX_VALUE;
        for (SensorData data : sensorData) {
            if (min > data.getTimestamp()) min = data.getTimestamp();
        }
        return min;
    }

    public void addSensorData(Collection<SensorData> sensorData) {
        if (firstTimestamp == 0) {
            firstTimestamp = minByTimeStamp(sensorData);
        }

        Map<Double, Collection<DataPoint>> mapByTimeStamp = Observable.fromIterable(sensorData)
                .map(o -> new DataPoint((double) (o.getTimestamp() - firstTimestamp) / 1000, o.getValue()))
                .toMultimap(DataPoint::getX, o -> o, HashMapSupplier.asCallable(), ArrayListSupplier.asFunction())
                .blockingGet();

        ArrayList<Double> orderedKeys = new ArrayList<>(mapByTimeStamp.keySet());
        Collections.sort(orderedKeys);

        for (Double key : orderedKeys) {

            if (key > counterTimestamp) counterTimestamp = key;
            else continue;

            List<DataPoint> points = (List<DataPoint>) mapByTimeStamp.get(counterTimestamp);
            if (points == null || points.size() != seriesList.size()) break;

            for (int i = 0; i < points.size(); i++)
                seriesList.get(i).appendData(points.get(i), true,
                        DescriptionGraphs.MAX_VISIBLE_PERIOD / DescriptionGraphs.PERIOD_COMPUTE_AVG);

            if (graphView != null) {
                double highestValueX = seriesList.get(0).getHighestValueX();
                graphView.getViewport().setMaxX(highestValueX);
                graphView.getViewport().setMinX(highestValueX - DescriptionGraphs.MAX_VISIBLE_PERIOD / 1000 + 1);
//                graphView.getViewport().setMinX(seriesList.get(0).getLowestValueX());
            }
        }
    }
}

