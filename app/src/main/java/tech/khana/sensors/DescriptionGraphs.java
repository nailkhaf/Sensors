package tech.khana.sensors;

import android.graphics.Color;
import android.hardware.Sensor;

public class DescriptionGraphs {
    public static final int PERIOD_COMPUTE_AVG = 1000; // 1000 millis
    public static final int MAX_VISIBLE_PERIOD = 1 * 60 * 1000; // 1 min more clearly than 5 min

    public static final byte[] sensorTypes = new byte[]{
            Sensor.TYPE_ACCELEROMETER,
            Sensor.TYPE_GYROSCOPE,
            Sensor.TYPE_LINEAR_ACCELERATION,
            Sensor.TYPE_LIGHT,
//            Sensor.TYPE_AMBIENT_TEMPERATURE, // check that you have this sensor on the phone
//            Sensor.TYPE_ROTATION_VECTOR
    };
    public static final String[] sensorNames = new String[]{
            "Accelerometer",
            "Gyroscope",
            "Linear acceleration",
            "Light",
//            "Temperature",
//            "Rotation vector"
    };
    public static final byte[] countDataType = new byte[]{
            3,
            3,
            3,
            1,
//            1,
//            3
    };
    public static final String[][] labels = new String[][]{
            {"x", "y", "z"},
            {"x", "y", "z"},
            {"x", "y", "z"},
            {"l"},
//            {"t"},
//            {"x", "y", "z"}
    };
    public static final int[][] colors = new int[][]{
            {Color.RED, Color.BLUE, Color.MAGENTA},
            {Color.RED, Color.BLUE, Color.MAGENTA},
            {Color.RED, Color.BLUE, Color.MAGENTA},
            {Color.BLUE},
//            {Color.RED},
//            {Color.RED, Color.BLUE, Color.MAGENTA}
    };
}
