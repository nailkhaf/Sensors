package tech.khana.sensors;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

import tech.khana.sensors.service.BackgroundService;

@RunWith(AndroidJUnit4.class)
public class BackgroundServiceTest {

    @Rule
    public final ServiceTestRule serviceTestRule = new ServiceTestRule();

    @Test
    @Ignore
    public void backgroundService() throws TimeoutException {
        Intent serviceIntent = new Intent(InstrumentationRegistry.getTargetContext(), BackgroundService.class);

        serviceTestRule.startService(serviceIntent);

    }
}
